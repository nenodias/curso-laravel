<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Usuario;
use App\Perfil;
use MongoDB\MongoCollection;
use MongoDB\Driver\Command;
use MongoDB\BSON\ObjectId;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $retorno = Usuario::all();
        foreach ($retorno as $key => $user) {
            if($user && $user->perfil && array_key_exists('$id',$user->perfil)){
                $user->perfil = Perfil::find($user->perfil['$id']);
            }else{
                Log::info($user);
            }
        }
        return $retorno;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dados = $request->all();
        $this->resolverPerfil($dados);
        return Usuario::create($dados);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Usuario::find($id);
        if($user){
            $user->perfil = Perfil::find($user->perfil['$id']);
            return $user;
        }
        
        return response('{"message":"NOT FOUND"}', 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Usuario::find($id);
        if($user){
            $dados = $request->all();
            $this->resolverPerfil($dados);
            $user->update($dados);
            return Usuario::find($id);
        }
        
        return response('{"message":"NOT FOUND"}', 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Usuario::find($id);
        if($user){
            $user->delete();
            return response('{"message":"OK"}', 200);
        }
        return response('{"message":"NOT FOUND"}', 404);
    }

    private function resolverPerfil($dados)
    {
        if(array_key_exists('id_perfil', $dados)){
            $perfil = [
                '$ref' => 'perfil',
                '$id' => new ObjectId($dados['id_perfil'])
            ];
            unset($dados['id_perfil']);
            $dados['perfil'] = $perfil;
        }else{
            unset($dados['perfil']);
        }
    }
}
