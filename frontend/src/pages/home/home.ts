import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { PerfilProvider } from '../../providers/perfil/perfil';
import { Camera, CameraOptions } from '@ionic-native/camera';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [
    UsuarioProvider,
    PerfilProvider,
    Camera
  ]
})
export class HomePage {

  public usuarios = [];

  public listaPerfil = [];

  public usuarioCadastro = {
    "_id": "",
    "nome": "",
    "idade": "",
    "perfil": "",
    "foto": ""
  };

  constructor(public navCtrl: NavController, private usuarioService: UsuarioProvider, private perfilService: PerfilProvider, private camera: Camera) {
    this.getUsuarios();
    this.getPerfis();
  }

  public getPerfis() {
    this.perfilService.findAll().subscribe(response => this.listaPerfil = response);
  }

  public getUsuarios() {
    this.usuarioService.findAll().subscribe(response => this.usuarios = response);
  }

  public limparForm() {
    this.usuarioCadastro.nome = "";
    this.usuarioCadastro.idade = "";
    this.usuarioCadastro._id = "";
    this.usuarioCadastro.perfil = "";
    this.usuarioCadastro.foto = "";
  }

  public salvar() {
    let dadosPreenchidos = this.usuarioCadastro.nome !== "" && this.usuarioCadastro.idade != "";
    if (this.usuarioCadastro._id === "" && dadosPreenchidos) {
      this.usuarioService.salvar(this.usuarioCadastro).subscribe(response => this.getUsuarios());
      this.limparForm();
    } else if (dadosPreenchidos) {
      this.usuarioService.editar(this.usuarioCadastro).subscribe(response => this.getUsuarios());
      this.limparForm();
    }
  }

  public deletar(id) {
    this.usuarioService.deletar(id).subscribe(response => this.getUsuarios());
  }

  public compareFn(e1, e2): boolean {
    return e1 && e2 ? e1.id === e2.id : e1 === e2;
  }

  public tirarFoto() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.usuarioCadastro.foto = base64Image;
      console.log(base64Image);
    }, (err) => {
      // Handle error
    });
  }

  public popularForm(usuario) {
    this.usuarioCadastro = usuario;
  }
}
